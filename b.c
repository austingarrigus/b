#include <stdio.h>

#define SYS_PATH "/sys/class/power_supply/BAT0/capacity"
#define NBAT path[27]

int main()
{
	FILE *fp;
	register int total = 0;
	char path[] = SYS_PATH;

	for (; fp = fopen(path, "r"); NBAT++) {
		int tmp;
		fscanf(fp, "%d", &tmp);
		printf("BAT%c: %d%%\n", NBAT, tmp);
		total += tmp;
		fclose(fp);
	}

	int nbat = NBAT - '0';
	switch (nbat) {
	case 0:
		printf("No batteries detected\n");
		break;
	case 1:
		break;
	default:
		total = total / nbat;
		printf(" avg: %d%%\n", total);
	}
	return 0;
}
