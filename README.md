# b

Displays current battery percentage for each battery. If multiple are present, displays the average as well.

## Installation

```
cc -o b b.c
sudo mv b /usr/local/bin/b
```


Or stick it wherever you want. I put it in ~/.local/bin/ and added that to my PATH
